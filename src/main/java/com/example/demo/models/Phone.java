package com.example.demo.models;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;

@Entity
@Data
@Getter
@Setter
public class Phone {

    @Id
    @GeneratedValue
    private Long id;
    private String modelName;
    private String brand;
    private Long price;
}
