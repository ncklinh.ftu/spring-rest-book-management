package com.example.demo.controller;

import com.example.demo.models.Phone;
import com.example.demo.models.User;
import com.example.demo.pojo.Book;
import com.example.demo.repositories.PhoneRepository;
import com.example.demo.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class MyController {

    @GetMapping("/sum")
    Integer sum(@RequestParam("a") Integer one,
                @RequestParam("b") Integer two) {
        return one + two;
    }

    @GetMapping("/factorial")
    Integer getFactorial(@RequestParam("a") Integer a) {
        int fact = 1;
        for (int i = 1; i <= a; i++) {
            fact *= i;
        }
        return fact;
    }

    @PostMapping("/save-book")
    String saveBook(@RequestBody Book book) {
        return book.getName();
    }

    @Autowired
    UserRepository userRepository;

    @PostMapping("/test")
    void test(@RequestBody User user) {
        userRepository.save(user);
    }

    @Autowired
    PhoneRepository phoneRepository;

    @PostMapping("/save-phone")
    void savePhone(@RequestBody Phone phone) {
        phoneRepository.save(phone);
    }
}
